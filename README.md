# README #

Install the following node packages  
npm install body-parser  
npm install express  
npm install mongoose  

For development, recommend:
npm install nodemon

To Run:    
node server.js  

To use nodemon:  
nodemon servevr.js  

## What is this repository for? ###  

A DnD web app that allows you to store, load, and modify characters.